
const api = 'https://mono-api.herokuapp.com';

const juego = new Vue({
    el: '#main',
    data: {
        login: false,
        banco: {
            login: false,
            capital: null,
            transferencia: {
                jugador: null, 
                monto: null
            }
        },
        idGame: null,
        jugador: null,
        clave: null,
        capital: null,
        jugadores: [],
        transferencia: {
            jugador: null,
            monto: null,
        },
        transferencias: [],
        ultimoJuego: null,
        log: [],
        socket: false
    },
    updated(){
        M.AutoInit();
    },
    mounted() {
        io.sails.url = api; 
        io.socket.get('/games', function(response){
            console.log('Conectado...');
        });
        
	    io.socket.on('connect', function(socket){
            juego.verificaCapital();
	        juego.socket = true;
        });
        
        io.socket.on('games', function(response){
            if (response.verb === "created") {
                juego.estableceJugador(response.data);
                juego.estableceBanco(response.data);
            }
            if (response.verb === "updated"){
                switch(response.data.accion){
                    case 'login':
                        juego.estableceJugador(response.data);
                    break;
                    case 'transfer':
                        juego.restableceSaldo(response.data);
                    break;
                    case 'logout':
                        juego.limpiar(response.data);
                        // TODO: Depura la lista de jugadores activos
                    break;
                    default: console.log(response.data.accion);
                }
            }
        });
        
        io.socket.on('disconnect', function(){
            juego.socket = false;
            io.socket.on('connect', function(){
                juego.verificaCapital();
            });
        });
        
        if ((sessionStorage.getItem('login')) == "true") {
            this.login = true;
            if (sessionStorage.banco) {
                this.banco.login = true;
                this.banco.capital = sessionStorage.banco;
            }
            this.capital = sessionStorage.capital;
            this.jugador = sessionStorage.jugador;
            this.clave = sessionStorage.clave;
            this.idGame = sessionStorage.idGame;
            this.jugadores = JSON.parse(sessionStorage.jugadores);
        }
    },
    methods: {
        crear() {
            io.socket.post('/game', {
                jugador: this.jugador,
                clave: this.clave
            }, function(data){
                if (data.estado === false) {
                    M.toast({html: data.mensaje},3000);
                }
            });
        },
        entrar() {
            if (this.clave === this.ultimoJuego) {
                io.socket.post('/game/in-again', {
                    jugador: this.jugador,
                    clave: this.clave
                }, function(data){
                    this.log.push(data);
                    if (data.estado === false) {
                        M.toast({html: data.mensaje},3000);
                    }
                });
            } else {
                io.socket.post('/game/in', {
                    jugador: this.jugador,
                    clave: this.clave
                }, function(data){
                    if (data.estado === false) {
                        M.toast({html: data.mensaje},3000);
                    }
                });
            }
        },
        estableceJugador(json){
            if (json.jugador.jugador === this.jugador && this.clave === json.clave) {
                this.capital = json.jugador.capital;
                this.idGame = json.id;
                this.login = true;
                sessionStorage.setItem('login', true);
                sessionStorage.setItem('idGame', json.id);
                sessionStorage.setItem('jugador', json.jugador.jugador);
                sessionStorage.setItem('clave', json.clave);
                sessionStorage.setItem('capital', json.jugador.capital);
            } else {
                if (this.clave === json.clave) M.toast({html: json.jugador.jugador+" ha ingresado"},3000);
            }
            if (json.clave === this.clave) {
                this.jugadores = json.jugadores;
                sessionStorage.setItem('jugadores', JSON.stringify(json.jugadores));
            }
        },
        estableceBanco(json){
            if (json.jugador.jugador === this.jugador && json.banco) {
                this.banco.login = true;
                this.banco.capital = json.jugador.banco.capital;
                sessionStorage.setItem('banco', json.jugador.banco.capital);
                M.toast({html: "Banco ha ingresado"},3000);
            }
        },
        restableceSaldo(json) {
            if (this.banco.login && json.origen === 'Banco') {
                M.toast({html: "Banco ha transferido $"+json.monto+" a "+json.destino},3000);
                this.registro("Enviado", json);
                this.banco.capital = parseInt(this.banco.capital) - parseInt(json.monto);
                this.banco.transferencia.jugador = null;
                this.banco.transferencia.monto = null;
                sessionStorage.setItem('banco', json.saldo.capital);
            } else if (json.origen === this.jugador && this.clave === json.clave) {
                M.toast({html: "Has transferido $"+this.transferencia.monto+" a "+this.transferencia.jugador},3000);
                this.registro("Enviado", json);
                this.capital = parseInt(this.capital) - parseInt(this.transferencia.monto);
                this.transferencia.jugador = null; 
                this.transferencia.monto = null;
                sessionStorage.setItem('capital', json.saldo.capital);
            }

            if (this.banco.login && json.destino === 'Banco') {
                this.registro("Recibido", json);
                if (this.clave === json.clave) M.toast({html: "Banco ha recibido $"+json.monto+" de "+json.origen},3000);
                this.banco.capital = parseInt(this.banco.capital) + parseInt(json.monto);
                sessionStorage.setItem('banco', this.banco.capital);
            } else if ( json.destino === this.jugador && this.clave === json.clave ) {
                this.registro("Recibido", json);
                if (this.clave === json.clave) M.toast({html: "Has recibido $"+json.monto+" de "+json.origen},3000);
                this.capital = parseInt(this.capital) + parseInt(json.monto);
                sessionStorage.setItem('capital', this.capital);
            }
        },
        verificaCapital(){
            if (juego.login) {
                axios.put(api+'/game/'+juego.idGame, {
                    banco: juego.banco.login,
                    jugador: juego.jugador
                }).then(function(capital){
                    if (juego.banco.login) {
                        juego.banco.capital = capital.data.banco;
                        sessionStorage.setItem('banco', capital.data.banco);
                    }
                    juego.capital = capital.data.jugador;
                    sessionStorage.setItem('capital', capital.data.jugador);
                });
            }
        },
        salir() {
            io.socket.delete('/game', {
                clave: this.clave,
                jugador: this.jugador
            }, function(data){
                console.log('[POST][Socket][salir()]: ', data);
            });
        },
        limpiar(json) {
            if (json.jugador === this.jugador && json.clave === this.clave) {
                this.ultimoJuego = json.clave;
                window.sessionStorage.clear();
                this.login = false;
                this.jugador = null;
                this.clave = null;
                this.capital = null;
                this.idGame = null;
                this.jugadores = null;
            } else {
                if (this.clave === json.clave) M.toast({html: json.jugador+" se ha desconectado"},3000);
            }
        },
        registro(operacion, json) {
            if (operacion === 'Enviado') {
                var detallesTransferencia = {
                    jugador: this.transferencia.jugador,
                    monto: this.transferencia.monto,
                    hora: new Date(),
                    operacion: operacion
                };
            } else {
                var detallesTransferencia = {
                    jugador: json.origen,
                    monto: json.monto,
                    hora: new Date(),
                    operacion: operacion
                };
            }
            this.transferencias.push(detallesTransferencia);
        },
        transferir: function(event, emisor) {
            let monto = this.transferencia.monto;
            let capital = this.capital;
            let origen = this.jugador;
            let destino = this.transferencia.jugador;
            if (emisor === 'banco'){
                monto = this.banco.transferencia.monto;
                capital = this.banco.capital;
                origen = 'Banco';
                destino = this.banco.transferencia.jugador;
            }

            if ( (monto > capital) || (!destino) || (!monto) ) {
                M.toast({html: "¡Datos requeridos!"},3000);
            } else {
                io.socket.post('/game/'+this.idGame+'/transfer', {
                    origen: origen,
                    destino: destino,
                    monto: monto
                }, function(data){
                    console.log('['+origen+'][POST][Socket][transferir()]: ', data);
                });
            }
        },
    },
    computed: {
        formatoCapital() {
            return Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(this.capital);
        },
        transaccionesRecientes() {
            return this.transferencias.sort((a, b) => b.hora - a.hora);
        }
    }
});