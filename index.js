var express = require('express');
var app = express();

var path = __dirname + '/';
app.use(express.static(path));

app.get('/', (req, res) => res.sendFile(path+'index.html'))
app.listen(80, () => console.log('Example app listening on port 80!'))